#odfiltruje z tablicy numArr liczby, ktore nie dziela sie bez reszty przez n
#zwraca nowa tablice z wynikami

def isDivByN(numArr, n):
    return filter(lambda num: not num % n, numArr)

numbers = [1,2,3,4,5,6,7]
n = 2

print "Liczby, ktore dziela sie przez %d : %s" % (n, isDivByN(numbers, n))